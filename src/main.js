// Imports for parcel
import 'animate.css'
import './main.scss'

// Constants for Google Maps API
const apiKey = 'AIzaSyBPWwqVaGNzxWQr_zbKe2jjzPArlBV0xZQ'
const geocodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address'
const tzUrl = 'https://maps.googleapis.com/maps/api/timezone/json?'

// Get local time 
function getCurTime(latLng) {  
  let cDate = new Date()  // get current date
  let tStamp = cDate.getTime() / 1000 + cDate.getTimezoneOffset() * 60  // get GMT time in seconds
  let apiUrl = `${tzUrl}location=${latLng}&timestamp=${tStamp}&key=${apiKey}`
  let offset, lTime

  // Vanilla JS fetch
  fetch(apiUrl)
    .then(resp => resp.json())
    .then((data) => {
      offset = data.dstOffset * 1000 + data.rawOffset * 1000  // Combine DST and timezone offsets in ms
      lTime = new Date(tStamp * 1000 + offset) // Combine offsets with GMT in ms 
      
      // Call function with location and hour in 24 hr notation (1..24) as arguments
      checkTimeOfDay(loc.value, lTime.getHours())      
    })
    .catch(error => console.log(error)) // Return error if promise fails
}

function getLatLong(location) {
  let apiUrl = `${geocodeUrl}=${location}&key=${apiKey}`
  fetch(apiUrl)
    .then(resp => resp.json())
    .then((data) => {
      let lat = data.results[0].geometry.location.lat
      let lng = data.results[0].geometry.location.lng
      let coords = `${lat},${lng}`      
      getCurTime(coords)
    })  
    .catch((error) => {
      console.log(error)
    })          
}

function checkTimeOfDay(location, time) {
  let fullHeightElm = document.getElementById('bgGradientChange')
  let showLocation = document.getElementById('showTime')
  let heroClassList = ['night', 'evening', 'morning', 'afternoon', 'is-primary', 'animated', 'fadeIn']

  if (time >= 5 && time <= 11) {    
    fullHeightElm.classList.remove(...heroClassList)
    fullHeightElm.classList.add('animated', 'fadeIn', 'morning')
    showLocation.innerHTML = `<h3 class="title animated fadeIn">Good morning, ${location}!</h3>`
  } else if (time >= 12 && time <= 16) {
      fullHeightElm.classList.remove(...heroClassList)
      fullHeightElm.classList.add('animated', 'fadeIn', 'afternoon')
      showLocation.innerHTML = `<h3 class="title animated fadeIn">Good afternoon, ${location}!</h3>`
  } else if (time >= 17 && time <= 19) {
      fullHeightElm.classList.remove(...heroClassList)
      fullHeightElm.classList.add('animated', 'fadeIn', 'evening')
      showLocation.innerHTML = `<h3 class="title has-text-light animated fadeIn">Good evening, ${location}!</h3>`
  } else {
      fullHeightElm.classList.remove(...heroClassList)
      fullHeightElm.classList.add('animated', 'fadeIn', 'night')
      showLocation.innerHTML = `<h3 class="title has-text-light animated fadeIn">Good night, ${location}!</h3>`
  }
}

document.getElementById('getTime').addEventListener('click', () => {
  let loc = document.getElementById('loc').value
  
  getLatLong(loc)
})

window.onload(document.getElementById('loc').value = '')